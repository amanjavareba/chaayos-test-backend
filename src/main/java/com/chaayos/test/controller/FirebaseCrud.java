package com.chaayos.test.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.chaayos.test.modal.TransactionEntity;
import com.chaayos.test.servce.FirebaseService;

@RestController
@RequestMapping(value="/api/firbease")
public class FirebaseCrud {
	@Autowired
	FirebaseService Service;
	
	@RequestMapping(value="/insert",method=RequestMethod.POST)
	public ResponseEntity<?> test(@RequestBody TransactionEntity entity) throws Exception{
		TransactionEntity resp=Service.insertData(entity);
		return new ResponseEntity<TransactionEntity>(resp,HttpStatus.OK);
	}

}
