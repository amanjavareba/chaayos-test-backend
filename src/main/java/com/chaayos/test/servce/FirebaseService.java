package com.chaayos.test.servce;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.chaayos.test.modal.TransactionEntity;
import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QuerySnapshot;

@Service
public class FirebaseService {
	@Resource
	@Qualifier("fs")
	Firestore fs;
	
	
	public TransactionEntity insertData(TransactionEntity entity) throws InterruptedException, ExecutionException {
		CollectionReference ref=fs.collection("chaayosAssignment");
		ApiFuture<DocumentReference> result = ref.add(entity);
		result.get().getId();
		return entity;
	}
	
	public long getDocsCount() throws InterruptedException, ExecutionException {
		CollectionReference ref=fs.collection("chaayosAssignment");
		ApiFuture<QuerySnapshot> result = ref.get();
		long count=result.get().size();
		return count;
	}
}
