package com.chaayos.test.config;

import static org.assertj.core.api.Assertions.catchThrowable;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.Firestore;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;

@Configuration
public class FireaseConf {
	@Bean(name="fs", autowire=Autowire.BY_NAME)
	public Firestore firebaseDatabase() {
		return FirestoreClient.getFirestore();
	}
	
	@PostConstruct 
    public void init() {
		FileInputStream refreshToken;
		FirebaseOptions option;
		try {
			refreshToken = new FileInputStream("src/main/resources/firebase-token.json");
			option=new FirebaseOptions.Builder()
					.setCredentials(GoogleCredentials.fromStream(refreshToken))
					.build();
			FirebaseApp.initializeApp(option);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}catch(IOException e) {
			
		}
    }
	
	
}
